angular
  .module('Acumulador', [])
  .controller("AcumCtrl", controladorPrincipal);

function controladorPrincipal(){
  //esta función es mi controlador
  var scope = this;
  scope.total = 0;
  scope.valor = 0;

  scope.sumar = function(){
    scope.total += parseInt(scope.valor);
  }
  scope.restar = function(){
    scope.total -= parseInt(scope.valor);
  }
};
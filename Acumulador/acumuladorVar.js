var app = angular.module('Acumulador',[]);
app.controller("AcumCtrl",["$scope", function($scope){
	$scope.total=0;
	$scope.valor=0;

	$scope.sumar = function(){
		$scope.total += parseInt($scope.valor);
	}
	$scope.restar = function(){
		$scope.total -= parseInt($scope.valor);
	}
}]);
